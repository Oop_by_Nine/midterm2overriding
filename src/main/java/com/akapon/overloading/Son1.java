/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.overloading;

/**
 *
 * @author Nine
 */
public class Son1 extends Mother{
    public Son1(String name,String speak){
        super(name,speak);
        
    }
    @Override
    public void speak(){
        System.out.println("Son name: " + this.name );
        System.out.println("Mother tell son eat lunch");
        System.out.println("Son tell Mother: " + this.speak );
    }
}
   